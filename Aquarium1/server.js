var http = require('http');
var schedule = require('node-schedule');
var WebSocketServer = require("ws").Server
var express = require("express")
var uwp = require("uwp");
var moment = require('moment');

uwp.projectNamespace("Windows");

var app = express()
var port = process.env.PORT || 5000

var status = 0;
var socketPin1 = 16;
var socketPin2 = 20
var socketPin3 = 21;

var gpioController = Windows.Devices.Gpio.GpioController.getDefault();
var socketPin1 = gpioController.openPin(16);
var socketPin2 = gpioController.openPin(20)
var socketPin3 = gpioController.openPin(21);

socketPin1.setDriveMode(Windows.Devices.Gpio.GpioPinDriveMode.output);
socketPin2.setDriveMode(Windows.Devices.Gpio.GpioPinDriveMode.output);
socketPin3.setDriveMode(Windows.Devices.Gpio.GpioPinDriveMode.output);

var highValue = Windows.Devices.Gpio.GpioPinValue.high;
var lowValue = Windows.Devices.Gpio.GpioPinValue.low;

var wsHolders = [];

var triggerPins = function () {
	var now = moment();
	var hour = now.hour();
	if (hour > 10 && hour < 22) {
		socketPin1.write(lowValue);
		socketPin2.write(lowValue);
		socketPin3.write(lowValue);
	} else {
		socketPin1.write(highValue);
		socketPin2.write(highValue);
		socketPin3.write(highValue);
	}
}

triggerPins();

var jobDay = schedule.scheduleJob('*/5 * * * * *', function () {
	triggerPins();
	broadcast(collect());
});

app.use(express.static(__dirname + "/"))

var server = http.createServer(app)
server.listen(port)

console.log("http server listening on %d", port)

var wss = new WebSocketServer({ server: server })
wss.broadcast = function broadcast(data) {
	wss.clients.forEach(function each(client) {
		client.send(JSON.stringify(data), function () { });
	});
};

var broadcast = function(data) { 
	wss.broadcast(data)
}

var collect = function () {
	return {
		date: moment(),
		s1: socketPin1.read(),
		s2: socketPin1.read(),
		s3: socketPin1.read(),
	}
}

wss.on("connection", function (ws) {
	ws.send(JSON.stringify(collect()), function () { })
	console.log("websocket connection open")
});